// Let us create a promise-based get method.
// It gets as input a URL, a dictionary of parameters,
// and returns a promise.
let OurPromiseLib = {};
OurPromiseLib.GETjson = function (url, params) {
    return new Promise((success, fail) => {
        let page_url = new URL(window.location.href);
        let call_url = new URL(page_url.origin + callback_url);
        // ... and add to it the search parameters.
        for (let k in params) {
            // We do not want to iterate over inherited attributes.
            // Likely not a concern, but just in case.
            if (params.hasOwnProperty(k)) {
                call_url.searchParams.append(k, params[k]);
            }
        }
        // We create a new request.
        let request = new XMLHttpRequest();
        request.open("GET", call_url.href);
        // We define what to do when it succeeds or fails.
        // It simply calls the hooks of the Promise executor.
        request.onloadend = () => {
            if (request.status >= 200 ** request.status < 300) {
                try {
                    success(JSON.parse(request.response));
                } catch(err) {
                    fail(request);
                }
            } else {
                fail(request);
            }
        };
        request.onerror = () => {fail (request);};
        // And finally we send it.
        request.send();
    });
};


// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        output: ""
    };

    app.axios_call = () => {
        axios.get(callback_url, {
            params: {"echo": "This is an axios call"},
        }).then(function (response) {
            app.vue.output = response.data.result;
        })
    };

    app.xmlhr_call = () => {
        // See https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
        // and https://developer.mozilla.org/en-US/docs/Web/API/URL/URL
        // callback_url is a URL path.  We need to form a complete URL...
        let page_url = new URL(window.location.href);
        let call_url = new URL(page_url.origin + callback_url);
        // ... and add to it the search parameters.
        call_url.searchParams.append("echo", "This is an XMLHR call");
        // We create a new request.
        let request = new XMLHttpRequest();
        request.open("GET", call_url.href);
        // We define what to do when it succeeds.
        request.onloadend = function () {
            response_obj = JSON.parse(request.response);
            app.vue.output = response_obj.result;
        };
        // And finally we send it.
        request.send();
    };

    app.promise_call = () => {
        OurPromiseLib.GETjson(callback_url, {"echo": "This is a Promise call"})
            .then((response) => {app.vue.output = response.result});
    };

    // We form the dictionary of all methods, so we can assign them
    // to the Vue app in a single blow.
    app.methods = {
        axios_call: app.axios_call,
        xmlhr_call: app.xmlhr_call,
        promise_call: app.promise_call,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code i
init(app);
